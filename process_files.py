import requests
from pathlib import Path

headers = {'Content-Type':'application/fhir+xml;fhirVersion=4.0.1',
           'Accept':'application/fhir+xml;fhirVersion=4.0.1'}

INPUT_DIR = 'cda_2_convert'
CONVERTED_DIR = 'converted_cda'
OUTPUT_DIR = 'created_fhir'

pathlist = Path(INPUT_DIR).glob('**/*.xml')
for path in pathlist:
  # because path is object not string
  path_in_str = str(path)
  print(path_in_str)
  with open(path_in_str, 'rb') as cda_document:
    res = requests.post('http://docker:8081/matchbox/fhir/StructureMap/$transform?source=http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureMap/at-cda-to-bundle', headers=headers, data=cda_document, verify=False)

  #print(res.text)
  res.raise_for_status()

  output_path = path_in_str.replace(INPUT_DIR, OUTPUT_DIR)
  with open(output_path, 'w', encoding='utf-8') as fhir_document:
    fhir_document.write(res.text)

  converted_path = path_in_str.replace(INPUT_DIR, CONVERTED_DIR)
  Path(path_in_str).rename(converted_path)
