import requests
from pathlib import Path

headers = {'Content-Type':'text/fhir-mapping',
           'Accept':'application/fhir+xml;fhirVersion=4.0.1'}

# upload datatype mapping
with open('./CDA2FHIR/maps/CdaToFhirTypes.map', 'rb') as maps:
  res = requests.post('http://docker:8081/matchbox/fhir/StructureMap', headers=headers, data=maps, verify=False)
  #print(res.text)
  res.raise_for_status()

# upload CDA2FHIR mapping
with open('./CDA2FHIR/maps/CdaToBundle.map', 'rb') as maps:
  res = requests.post('http://docker:8081/matchbox/fhir/StructureMap', headers=headers, data=maps, verify=False)
  #print(res.text)
  res.raise_for_status()
